﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Collections.Specialized;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860


public class DataBaseConnection
{
    protected SqlConnection con = new SqlConnection();


    protected DataBaseConnection()
    {
        con.ConnectionString = ConfigurationManager.ConnectionStrings["CourierWorldContext"].ConnectionString;
        con.Open();
    }
    protected void OpenConnection()
    {
        if (con.State == ConnectionState.Closed)
            con.Open();

    }
    protected void CloseConnection()
    {
        if (con.State == ConnectionState.Open)
            con.Close();
        con.Dispose();
    }
    public static string Connection()
    {
        string s = string.Empty;
        s = ConfigurationManager.ConnectionStrings["CourierWorldContext"].ConnectionString;
        return s;
    }

}
