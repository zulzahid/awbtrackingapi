﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.ApplicationBlocks.Data;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AWBTrackingAPI.Data
{
    public class Class_DataModel : DataBaseConnection
    {

        DataSet ds = new DataSet();
        SqlParameter[] param;

        public DataSet VerifyTokenId(string acsessToken)
        {
            param = new SqlParameter[1];
            param[0] = new SqlParameter("@acsessToken", acsessToken);
            return ds = SqlHelper.ExecuteDataset(con, CommandType.Text, "select * from [API12].[Customer_auth] where token_access=@acsessToken", param);
        }



        public bool IsDataSetValidate(DataSet ds)
        {
            if (ds == null) { return false; }

            if (ds.Tables.Count == 0) { return false; }

            if (ds.Tables[0].Rows.Count == 0) { return false; }

            if (ds.Tables[0].Rows.Count > 0) { return true; }

            return false;
        }

        public IEnumerable<IDataRecord> GetSomeData(string fields, string table, string where, int count = 0)
        {
            string sql = "SELECT @Fields FROM @Table WHERE OriginStn = '@Where'";

            using (SqlCommand cmd = new SqlCommand(sql, con))
            {
                cmd.Parameters.AddWithValue("@Fields", fields);
                cmd.Parameters.AddWithValue("@Table", table);
                cmd.Parameters.AddWithValue("@Where", where);

                using (IDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        yield return (IDataRecord)rdr;
                    }
                }
            }
        }

    }
}
