﻿using AWBTrackingAPI.Dto;
using AWBTrackingAPI.Dto.SNT;
using AWBTrackingAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Service.SNT
{
    public interface ITracking3plService

    {
        Task<ResultAWBTrackingDto> CreateDelivery(TrackingRequestDto deliveryDto);

        ResultAWBTrackingDto Validate(TrackingRequestDto deliveryDto);

        DtTrackingEventDto GetTrackingByDriver(string accesstoken, string driver);

        DtTrackingEventDto GetTrackingCollectedByDriver(TrackingEventDto data);
    }
}
