﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using AWBTrackingAPI.Dto;
using AWBTrackingAPI.Dto.SNT;
using AWBTrackingAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace AWBTrackingAPI.Service.SNT
{
    public class Tracking3plService : ITracking3plService
    {
        private readonly DataContext context;


        public Tracking3plService(DataContext context)
        {
            this.context = context;
        }

        public ResultAWBTrackingDto Validate(TrackingRequestDto snorderrequest)
        {
            //SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["FullConnectionString"].ConnectionString);
            string CustomerAccountNo = "";
            int notInserted = 0;
            int inserted = 0;

            DataSet ds = new DataSet();
            List<Awbtracking> AWBModelList = snorderrequest.data;

            if (snorderrequest.data == null)
            {
                return new ResultAWBTrackingDto("ERROR", 400, "Invalid customer account");
            }
            else if (snorderrequest.access_token == null || snorderrequest.access_token == string.Empty)
            {
                return new ResultAWBTrackingDto("ERROR", 400, "Invalid customer account");
            }
            else if (string.IsNullOrWhiteSpace(snorderrequest.access_token))
            {
                return new ResultAWBTrackingDto("ERROR", 401, "Invalid Token!");
            }
            else if (checkToken(snorderrequest.access_token))
            {
                return new ResultAWBTrackingDto("ERROR", 401, "Invalid Token!");
            }
            else if (snorderrequest.data != null)
            {
                string message = "";
                string code = "";
                string status = "";
                decimal? Weight = new decimal();
                decimal weightcon = new decimal();
                String weightstr = "";
                decimal? WeightVol = new decimal();
                decimal weightVolconv = new decimal();
                string WeightVol2Dec = "";

                try
                {
                    var eventType = new List<string> { "C.FEEDBACK", "CHECKIN", "CONSOLE DELETED", "HANDOVER", "HUB CHECKIN", "HUB LINEHAUL", "HUB MANIFEST", "HUB OBC-MANIFEST", "ITEM REMOVED", "LINEHAUL", "MANIFEST", "MRCHECIN", "NO SHOW", "OBC CHECK IN", "OFD", "POD", "PROBLEM", "PUCHECIN", "RECEIVED", "RTS", "DROPOFF", "BAGGED", "COLLECTED" };


                    int dataCount = AWBModelList.Count();

                    for (int i = 0; i < dataCount; i++)
                    {
                        //if (AWBModelList[i].EventType != null && AWBModelList[i].EventType.Length > 50)
                        //{
                        //    return RetrunMsg(HttpStatusCode.OK, "EventType length exceed limit", "300");
                        //}

                        if (AWBModelList[i].Awbnumber != null && AWBModelList[i].Awbnumber.Length > 30 )
                        {

                            return new ResultAWBTrackingDto("ERROR", 402, "AWBNumber length less then minimum/ Exceed Limit");

                        }

                        if (AWBModelList[i].ShipmentType != null && (!AWBModelList[i].ShipmentType.Equals("DOCUMENT") && !AWBModelList[i].ShipmentType.Equals("PARCEL")))
                        {

                            return new ResultAWBTrackingDto("ERROR", 301, "Shipment Type is not valid");

                        }

                        //if (AWBModelList[i].ShipmentType != null && AWBModelList[i].ShipmentType.Length > 50 )
                        //{
                        //    return RetrunMsg(HttpStatusCode.OK, "Shipment Type length is exceed limit", "303");
                        //}

                        

                        if (AWBModelList[i].Weight != null && !AWBModelList[i].Weight.Equals(""))
                        {
                            Weight = AWBModelList[i].Weight;
                            weightcon = (decimal)Weight;
                            weightstr = weightcon.ToString();
                        }
                        else
                        {
                            Weight = new decimal(0.00);
                            weightcon = (decimal)Weight;
                            weightstr = weightcon.ToString("0.00");
                        }


                        if (!Weight.Equals("") && weightstr.IndexOf(".") == -1)
                        {
                            return new ResultAWBTrackingDto("ERROR", 302, "Weight is not a decimal");
                        }


                        

                        if (AWBModelList[i].WeightVol != null && !AWBModelList[i].WeightVol.Equals(""))
                        {
                            WeightVol = AWBModelList[i].Weight;
                            weightVolconv = (decimal)WeightVol;
                            WeightVol2Dec = weightVolconv.ToString();
                        }
                        else
                        {
                            WeightVol = new decimal(0.00);
                            weightVolconv = (decimal)WeightVol;
                            WeightVol2Dec = weightVolconv.ToString("0.00");
                        }

                        if (!WeightVol.Equals("") && WeightVol2Dec.IndexOf(".") == -1)
                        {
                            return new ResultAWBTrackingDto("ERROR", 302, "Weight Volume is not a decimal");
                        }

                        if (!WeightVol.Equals("") && WeightVol2Dec.IndexOf(".") == -1)
                        {
                            return new ResultAWBTrackingDto("ERROR", 302, "Weight is not a decimal");
                        }


                        if (AWBModelList[i].OriginStn != null && AWBModelList[i].OriginStn.Length > 20)
                        {
                            return new ResultAWBTrackingDto("ERROR", 304, "OriginStn length is exceed limit");
                        }

                        if (AWBModelList[i].DestStn != null && AWBModelList[i].DestStn.Length > 20)
                        {
                            return new ResultAWBTrackingDto("ERROR", 304, "DestStn length is exceed limit");
                        }

                        if (AWBModelList[i].TrackingNum != null && AWBModelList[i].TrackingNum.Length < 8 || AWBModelList[i].TrackingNum.Length > 30 )
                        {
                            return new ResultAWBTrackingDto("ERROR", 304, "TrackingNum length less then minimum/ Exceed Limit");
                        }

                        bool contains = eventType.Contains(AWBModelList[i].EventType, StringComparer.OrdinalIgnoreCase);
                        if (AWBModelList[i].EventType != null && !contains)
                        {
                            return new ResultAWBTrackingDto("ERROR", 303, "EventType is not valid");
                        }

                        
                        var items = context.CustomerAuth.Where(x => x.TokenAccess == snorderrequest.access_token)
                                     .Select(x => new
                                     {
                                         P1 = x.CustomerAccountNo,
                                        
                                     });

                        foreach (var ite in items)
                        {
                            CustomerAccountNo = ite.P1;
                        }

                       
                    }



                    var query = "";

                    for (int i = 0; i < snorderrequest.data.Count(); i++)
                    {
                        if (snorderrequest.data[i].WeightVol == null || snorderrequest.data[i].WeightVol.Equals(""))
                        {
                            snorderrequest.data[i].WeightVol = Decimal.Parse(WeightVol2Dec); ;
                        }

                        if (snorderrequest.data[i].Weight == null || snorderrequest.data[i].WeightVol.Equals(""))
                        {
                            snorderrequest.data[i].Weight = Decimal.Parse(weightstr); ;
                        }

                        //Prevent Multiple Insertion of Same AWB from different Station
                        var inq = new List<string> { "POD", "RTS", "COLLECTED", "DROPOFF", "PUCHECIN" };

                        //Checking if there is existing awbnumber
                        var checkEvent = from c in context.Awbtracking
                                                   where c.TrackingNum == AWBModelList[i].TrackingNum
                                                   select c.EventType;

                        bool RowExist = false;

                        //Checking if existing awbnumber contains same data
                        foreach (var itemf in checkEvent)
                        {
                            RowExist = inq.Any(str => str.Contains(AWBModelList[i].EventType));

                            if(RowExist == true)
                            {
                                break;
                            }
                        }                        

                        if (RowExist)
                        {
                            notInserted = notInserted + 1;

                            message = "DONE";

                            continue;
                        }
                        else
                        {
                            //eventType POD/RTS

                            if (snorderrequest.data[i].EventType != null && snorderrequest.data[i].EventType.Equals("POD") || snorderrequest.data[i].EventType.Equals("RTS"))
                            {
                                code = "305";
                                status = "POD/RTS EventType";

                                notInserted = notInserted + 1;
                                message = "DONE";
                                continue;
                            }
                            else
                            {
                                

                                if (snorderrequest.data[i].Awbdate.GetHashCode() != 0)
                                {

                                    //query = "INSERT INTO AWBTracking( StnTrackingId,TrackingNum,AWBNumber,AWBDate,ShipmentType,Weight,WeightVol,OriginStn,DestStn,CreatedBy,EventType,Reference,StnCode,IsTransfered,IsTransferedDes) VALUES ( @StnTrackingId,@TrackingNum,@AWBNumber,@AWBDate,@ShipmentType,@Weight,@WeightVol,@OriginStn,@DestStn,@CreatedBy,@EventType,@Reference,@StnCode,@IsTransfered,@IsTransferedDes);";

                                    context.Awbtracking.Add(
                                        new Awbtracking
                                        {
                                            TrackingNum = snorderrequest.data[i].TrackingNum,
                                            Awbnumber = snorderrequest.data[i].Awbnumber,
                                            Awbdate = snorderrequest.data[i].Awbdate,
                                            ShipmentType = snorderrequest.data[i].ShipmentType,
                                            Weight = snorderrequest.data[i].Weight,
                                            WeightVol = snorderrequest.data[i].WeightVol,
                                            OriginStn = snorderrequest.data[i].OriginStn,
                                            DestStn = snorderrequest.data[i].DestStn,
                                            CreatedBy = snorderrequest.data[i].CreatedBy,
                                            EventType = snorderrequest.data[i].EventType,
                                            Reference = snorderrequest.data[i].Reference + "-" + CustomerAccountNo + "- TrackingAPI",
                                            StnCode = snorderrequest.data[i].StnCode,
                                            IsTransfered = false,
                                            IsTransferedDes = false

                                        }
                                        );

                                    context.SaveChanges();

                                    //context.Database.ExecuteSqlCommand(query,
                                    //new SqlParameter("@StnTrackingId", "0"),
                                    //new SqlParameter("@TrackingNum", snorderrequest.data[i].TrackingNum),
                                    //new SqlParameter("@AWBNumber", snorderrequest.data[i].Awbnumber),
                                    //new SqlParameter("@AWBDate", snorderrequest.data[i].Awbdate),
                                    //new SqlParameter("@ShipmentType", snorderrequest.data[i].ShipmentType),
                                    //new SqlParameter("@Weight", snorderrequest.data[i].Weight),
                                    //new SqlParameter("@WeightVol", snorderrequest.data[i].WeightVol),
                                    //new SqlParameter("@OriginStn", snorderrequest.data[i].OriginStn),
                                    //new SqlParameter("@DestStn", snorderrequest.data[i].DestStn),
                                    //new SqlParameter("@CreatedBy", snorderrequest.data[i].CreatedBy),
                                    //new SqlParameter("@EventType", snorderrequest.data[i].EventType),
                                    //new SqlParameter("@Reference", snorderrequest.data[i].Reference + "-" + CustomerAccountNo + "- TrackingAPI"),
                                    //new SqlParameter("@StnCode", snorderrequest.data[i].StnCode),
                                    //new SqlParameter("@IsTransfered", '0'),
                                    //new SqlParameter("@IsTransferedDes", '0'));

                                    message = "DONE";

                                }
                                else
                                {
                                    // query = "INSERT INTO AWBTracking( StnTrackingId,TrackingNum,AWBNumber,AWBDate,ShipmentType,Weight,WeightVol,OriginStn,DestStn,CreatedBy,EventType,Reference,StnCode,IsTransfered,IsTransferedDes) VALUES (@StnTrackingId, @TrackingNum,@AWBNumber,GETDATE(),@ShipmentType,@Weight,@WeightVol,@OriginStn,@DestStn,@CreatedBy,@EventType,@Reference,@StnCode,@IsTransfered,@IsTransferedDes);";
                                    // context.Database.ExecuteSqlCommand(query,
                                    //new SqlParameter("@StnTrackingId", "0"),
                                    //new SqlParameter("@TrackingNum", snorderrequest.data[i].TrackingNum),
                                    //new SqlParameter("@AWBNumber", snorderrequest.data[i].Awbnumber),
                                    //new SqlParameter("@ShipmentType", snorderrequest.data[i].ShipmentType),
                                    //new SqlParameter("@Weight", snorderrequest.data[i].Weight),
                                    //new SqlParameter("@WeightVol", snorderrequest.data[i].WeightVol),
                                    //new SqlParameter("@OriginStn", snorderrequest.data[i].OriginStn),
                                    //new SqlParameter("@DestStn", snorderrequest.data[i].DestStn),
                                    //new SqlParameter("@CreatedBy", snorderrequest.data[i].CreatedBy),
                                    //new SqlParameter("@EventType", snorderrequest.data[i].EventType),
                                    //new SqlParameter("@Reference", snorderrequest.data[i].Reference + "-" + CustomerAccountNo + "- TrackingAPI"),
                                    //new SqlParameter("@StnCode", snorderrequest.data[i].StnCode),
                                    //new SqlParameter("@IsTransfered", '0'),
                                    //new SqlParameter("@IsTransferedDes", '0'));


                                    context.Awbtracking.Add(
                                       new Awbtracking
                                       {
                                           TrackingNum = snorderrequest.data[i].TrackingNum,
                                           Awbnumber = snorderrequest.data[i].Awbnumber,
                                           Awbdate = System.DateTime.Now,
                                           ShipmentType = snorderrequest.data[i].ShipmentType,
                                           Weight = snorderrequest.data[i].Weight,
                                           WeightVol = snorderrequest.data[i].WeightVol,
                                           OriginStn = snorderrequest.data[i].OriginStn,
                                           DestStn = snorderrequest.data[i].DestStn,
                                           CreatedBy = snorderrequest.data[i].CreatedBy,
                                           EventType = snorderrequest.data[i].EventType,
                                           Reference = snorderrequest.data[i].Reference + "-" + CustomerAccountNo + "- TrackingAPI",
                                           StnCode = snorderrequest.data[i].StnCode,
                                           IsTransfered = false,
                                           IsTransferedDes = false

                                       }
                                       );

                                    context.SaveChanges();

                                    message = "DONE";
                                }

                                inserted = inserted + 1;




                            }
                        }

                        //using (var command = context.Database.GetDbConnection().CreateCommand())
                        //{
                        //    command.CommandText = "select * from AWBTracking where TrackingNum = '" + AWBModelList[i].Awbnumber + "' and EventType in ('POD','RTS')";
                        //    context.Database.OpenConnection();
                        //    using (var result = command.ExecuteReader())
                        //    {
                        //        if (result.HasRows)
                        //        {
                        //            notInserted = notInserted + 1;

                        //        }
                        //        else
                        //        {
                                    
                        //        }
                        //    }
                        //}

                        

                        code = "200";
                        status = "success";
                       
                    }

                    if (message == "DONE")
                    {
                        status = "(" + inserted + ") Tracking Inserted / (" + notInserted + ") Tracking not inserted " ;

                    }

                    return new ResultAWBTrackingDto("SUCCESS", 200, status);


                }
                catch (Exception e)
                {
                    return new ResultAWBTrackingDto("ERROR", 501, e.ToString());

                }

            }
            else
            {
                return null;
            }

            

            
        }

        private bool checkToken(string token)
        {
            try{
                var savedtoken = context.CustomerAuth.FirstOrDefault(i => i.TokenAccess == token);
                //var secretkey = context.Auth.FirstOrDefault(i => i.SourceId == sourceId).SecretKey;

                if (savedtoken != null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine("Exception :" + e.ToString());
                return true;
            }
            
        }


        public async Task<ResultAWBTrackingDto> CreateDelivery(TrackingRequestDto snorderrequest)
        {
            foreach (var item in snorderrequest.data)
            {
                var tracking = new Awbtracking
                {
                    TrackingNum = item.TrackingNum,
                    Awbnumber = item.Awbnumber,
                    Awbdate = item.Awbdate,
                    ShipmentType = item.ShipmentType,
                    Weight = item.Weight,
                    WeightVol = item.WeightVol,
                    OriginStn = item.OriginStn,
                    DestStn = item.DestStn,
                    CreatedBy = item.CreatedBy,
                    EventType = item.EventType,
                    Reference = item.Reference,
                    StnCode = item.StnCode

            };

                await context.Awbtracking.AddAsync(tracking);

                await context.SaveChangesAsync();
            }
            return new ResultAWBTrackingDto("SUCCESS", 200, "Tracking Successfully Inserted");
        }


        public DtTrackingEventDto GetTrackingByDriver(string access_token, string driver)
        {

            if (checkToken(access_token))
            {
                return null;
            }
            else {
                var query = context.Awbtracking.Where(item => item.CreatedBy == driver && item.EventType.Contains("COLLECTED")).ToList();
                //string json = JsonConvert.SerializeObject(query);

                List<Awbtracking> dt = query;

                return new DtTrackingEventDto(dt);
            }


        }


        public DtTrackingEventDto GetTrackingCollectedByDriver(TrackingEventDto data)
        {
            if (checkToken(data.access_token))
            {
                return null;
            }
            else
            {
                var query = context.Awbtracking.Where(item => item.CreatedBy == data.driver && item.EventType.Contains("COLLECTED")).ToList();
                //string json = JsonConvert.SerializeObject(query);

                List<Awbtracking> dt = query;

                return new DtTrackingEventDto(dt);
            }
        }
    }
}
