﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Dto
{
    public class ResultAWBTrackingDto
    {
        public ResultAWBTrackingDto(string status, int code, string message)
        {
            Status = status;
            Code = code;
            Message = message;
        }

        public string Status { get; set; }
        public int Code { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
