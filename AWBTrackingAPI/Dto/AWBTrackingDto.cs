﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Dto
{
    public class AWBTrackingDto
    {
        //public long TrackingId { get; set; }
        //public long StnTrackingId { get; set; }
        public string TrackingNum { get; set; }
        public string AWBNumber { get; set; }
        public DateTime AWBDate { get; set; }
        public string ShipmentType { get; set; }
        public decimal Weight { get; set; }
        public decimal WeightVol { get; set; }
        public string OriginStn { get; set; }
        public string DestStn { get; set; }
        public string CreatedBy { get; set; }
        public string EventType { get; set; }
        public string Reference { get; set; }
        public string StnCode { get; set; }

        public static implicit operator AWBTrackingDto(List<object> v)
        {
            throw new NotImplementedException();
        }
        //public bool IsTransfered { get; set; }
        //public string ImportFile { get; set; }
        //public bool IsTransferedDes { get; set; }
        //public string ImportFileDes { get; set; }
        //public string SendToDest { get; set; }
        //public bool IsHUB { get; set; }
        //public string SendToDest2 { get; set; }
        //public bool IsTransfered2 { get; set; }
        //public DateTime PODDate { get; set; }
    }
}
