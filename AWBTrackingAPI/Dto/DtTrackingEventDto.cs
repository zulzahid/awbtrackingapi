﻿using AWBTrackingAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Dto
{
    public class DtTrackingEventDto
    {
        public DtTrackingEventDto(List<Awbtracking> awbTrackingDto)
        {
            AwbTrackingDto = awbTrackingDto;
        }


        public List<Awbtracking> AwbTrackingDto { get; set; }


        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
