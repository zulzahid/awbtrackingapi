﻿using AWBTrackingAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Dto
{
    public class TrackingRequestDto
    {
        public string access_token { get; set; }

        public List<Awbtracking> data { get; set; }
    }

    public class TrackingEventDto
    {
        public string access_token { get; set; }

        public string driver { get; set; }
    }
}
