﻿using System;
using System.Collections.Generic;

namespace AWBTrackingAPI.Models
{
    public partial class Awbtracking
    {
        public long TrackingId { get; set; }
        public long StnTrackingId { get; set; }
        public string TrackingNum { get; set; }
        public string Awbnumber { get; set; }
        public DateTime Awbdate { get; set; }
        public string ShipmentType { get; set; }
        public decimal? Weight { get; set; }
        public decimal? WeightVol { get; set; }
        public string OriginStn { get; set; }
        public string DestStn { get; set; }
        public string CreatedBy { get; set; }
        public string EventType { get; set; }
        public string Reference { get; set; }
        public string StnCode { get; set; }
        public bool IsTransfered { get; set; }
        public string ImportFile { get; set; }
        public bool IsTransferedDes { get; set; }
        public string ImportFileDes { get; set; }
        public string SendToDest { get; set; }
        public bool? IsHub { get; set; }
        public string SendToDest2 { get; set; }
        public bool? IsTransfered2 { get; set; }
        public DateTime? Poddate { get; set; }


    }
}
