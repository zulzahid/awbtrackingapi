﻿using System;
using System.Collections.Generic;

namespace AWBTrackingAPI.Models
{
    public partial class CustomerAuth
    {
        public int Id { get; set; }
        public string CustomerAccountNo { get; set; }
        public string TokenAccess { get; set; }
    }
}
