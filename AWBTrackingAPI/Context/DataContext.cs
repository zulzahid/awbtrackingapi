﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace AWBTrackingAPI.Models
{
    public partial class DataContext : DbContext
    {
        public DataContext()
        {
        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {
        }



        public virtual DbSet<Awbtracking> Awbtracking { get; set; }
        public virtual DbSet<CustomerAuth> CustomerAuth { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.3-servicing-35854");

            modelBuilder.Entity<Awbtracking>(entity =>
            {
                entity.HasKey(e => e.TrackingId);

                entity.ToTable("AWBTracking");

                entity.Property(e => e.Awbdate)
                    .HasColumnName("AWBDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Awbnumber)
                    .IsRequired()
                    .HasColumnName("AWBNumber")
                    .HasMaxLength(30);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.DestStn).HasMaxLength(20);

                entity.Property(e => e.EventType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.ImportFile).HasMaxLength(50);

                entity.Property(e => e.ImportFileDes).HasMaxLength(50);

                entity.Property(e => e.IsHub)
                    .HasColumnName("IsHUB")
                    .HasDefaultValueSql("((0))");

                entity.Property(e => e.IsTransfered2).HasDefaultValueSql("((0))");

                entity.Property(e => e.OriginStn).HasMaxLength(20);

                entity.Property(e => e.Poddate)
                    .HasColumnName("PODDate")
                    .HasColumnType("datetime");

                entity.Property(e => e.Reference).HasMaxLength(50);

                entity.Property(e => e.SendToDest).HasMaxLength(10);

                entity.Property(e => e.SendToDest2)
                    .HasMaxLength(12)
                    .IsUnicode(false);

                entity.Property(e => e.ShipmentType)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.StnCode).HasMaxLength(20);

                entity.Property(e => e.TrackingNum)
                    .IsRequired()
                    .HasMaxLength(30);

                entity.Property(e => e.Weight).HasColumnType("decimal(18, 3)");

                entity.Property(e => e.WeightVol).HasColumnType("decimal(18, 3)");
            });

            modelBuilder.Entity<CustomerAuth>(entity =>
            {
                entity.ToTable("Customer_auth", "API12");

                entity.Property(e => e.CustomerAccountNo)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.TokenAccess)
                    .IsRequired()
                    .HasColumnName("token_access")
                    .HasMaxLength(50)
                    .IsUnicode(false)
                    .HasDefaultValueSql("(newid())");
            });
        }
    }
}
