﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using AWBTrackingAPI.Data;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace AWBTrackingAPI.Controllers
{
    public class BaseApiController : ApiController
    {

        public Class_DataModel dbsql = new Class_DataModel();

        protected HttpResponseMessage BuildSuccessResult(HttpStatusCode statusCode)
        {
            return this.Request.CreateResponse(statusCode);
        }

        protected HttpResponseMessage BuildSuccessResult(HttpStatusCode statusCode, object data)
        {
            return data != null ? this.Request.CreateResponse(statusCode, data) : this.Request.CreateResponse(statusCode);
        }



        protected HttpResponseMessage BuildErrorResult(HttpStatusCode statusCode, string errorCode = null, string message = null)
        {
            return this.Request.CreateResponse(statusCode, new
            {
                ErrorCode = errorCode,
                Message = message
            });
        }

        public class MultipleObjects
        {
            //public object total_orders { get; set; }
            public object trackstatus { get; set; }
        }

        public class MultipleObjectsInvoice
        {
            public object total_orders { get; set; }
            public IEnumerable<object> InvoiceData { get; set; }
        }


        public class MultipleCommonObjects
        {
            public IEnumerable<object> response { get; set; }
        }

        //[HttpPost]
        public HttpResponseMessage RetrunMsg(HttpStatusCode msgstatus, string Msg, string code)
        {
            var response = new Dictionary<string, string>
            {
                { "code", code},
                { "message", Msg}
            };

            return BuildSuccessResult(msgstatus, response);
        }


    }
}
