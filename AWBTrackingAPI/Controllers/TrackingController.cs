﻿using AWBTrackingAPI.Dto;
using AWBTrackingAPI.Models;
using AWBTrackingAPI.Service.SNT;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AWBTrackingAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TrackingController : ControllerBase
    {

        private const string CreateAWBTracking = "CreateAWBTracking";
        private const string getTracking = "getTracking";
        private const string getTrackingCollectedByDriver = "getTrackingCollectedByDriver";


        private readonly ITracking3plService plService;

        public TrackingController(ITracking3plService plService)
        {
            this.plService = plService;
        }

        [HttpGet]
        public string Get()
        {
            return "Api Working 2.0";
        }

        [HttpPost(CreateAWBTracking)]
        public async Task<ResultAWBTrackingDto> createAWBTracking([FromBody]TrackingRequestDto orderRequestDto)
        {
            var result = plService.Validate(orderRequestDto);

            return result;
            
        }

        [HttpGet(getTracking)]
        public async Task<DtTrackingEventDto> getAWBTracking([FromQuery] string access_token, [FromQuery] string driver)
        {
            var result = plService.GetTrackingByDriver(access_token, driver);


            return result;

        }

        [HttpPost(getTrackingCollectedByDriver)]
        public async Task<DtTrackingEventDto> getAWBTrackingCollectedByDriver([FromBody] TrackingEventDto data)
        {
            var result = plService.GetTrackingCollectedByDriver(data);


            return result;

        }

        //[HttpPost(getTracking)]
        //public async Task<IActionResult> SendOrders([FromBody]TrackingRequestDto orderRequestDto)
        //{
        //    var result = await plService.SendRequest(orderRequestDto);

        //    if (result == null)
        //    {
        //        return BadRequest(new { message = "WrongToken" });
        //    }
        //    else
        //    {
        //        return Ok(result);
        //    }
        //}
    }


}